# If we have `--squash` support, then use it!
ifneq ($(shell docker build --help 2>/dev/null | grep squash),)
DOCKER_BUILD = docker build --squash
else
DOCKER_BUILD = docker build
endif

#DOCKER_BUILD += --network=host

all:
	$(MAKE) -C nginx-certbot/src
	$(DOCKER_BUILD) -t http_nginx nginx
	$(DOCKER_BUILD) --pull -t http_app app
