from flask import Flask, request
import requests
import sys
import pprint
import logging
import logging.config

import yaml
from log import MyLogRecord
from os.path import abspath
from pathlib import Path
d = Path(abspath(__file__)).parent

def setup_logging() -> None:
    with Path(d / 'logging.yaml').open('rt', encoding='utf-8') as f:
        cfg = yaml.load(f)
    logging.setLogRecordFactory(MyLogRecord)
    logging.config.dictConfig(cfg)

setup_logging()
# the all-important app variable:
app = Flask(__name__)
log = app.logger

@app.route("/")
def hello():
    return "Oh, Hello World"

allowed_actions = [
    'open', 'update'
# Disabled: 'reopen', 'close', 'merge'
]

@app.route('/gitlabhook', methods=['POST'])
def gitlabhook():
    log.debug('request')
    token = request.headers.get('X-Gitlab-Token')
    log.debug('token={}'.format(token))
    rq = request.json
    if rq['object_kind'] == 'merge_request':
        log.info('Merge request!')
        log.debug('arguments: {}'.format(pprint.pformat(rq)))
        attrs = rq['object_attributes']
        ref = attrs['source_branch']
        project_name = rq['project']["name"]
        project_id = rq['project']["id"]
        data = {
            'token': token,
            'ref': ref
        }
        #log('head_pipeline_id = ', attrs['head_pipeline_id'])
        action = attrs.get('action', '')
        log.info('action = {}'.format(action))
        #if attrs['head_pipeline_id'] is not None:
        #    log('head_pipeline_id present -> skipping')
        #    return ''
        if action not in allowed_actions:
            log.info('not an allowed action -> skipping')
            return ''
        r = requests.post("https://gitlab.fel.cvut.cz/api/v4/projects/{}/trigger/pipeline".format(project_id), json=data)
        log.debug(r.text)
        return r.text
    return 'Not processed'

log.info('start')
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False, port=80)
