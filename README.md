# Gitlab webhook <-> pipeline trigger middleware

Contains: nginx + letsencrypt HTTPS, uwsgi + Flask microapp

app must have access to internet -> hence the default network

Rebuild and update by: make && docker-compose up -d

View live logs by: docker-compose logs -f

## How to set up

1. Set up pipeline trigger (Settings - CI/CD).
2. Set up webhook (Settings - Integrations).  
   URL: https://SERVER/gitlabhook  
   Secret Token: *the token obtained from trigger setup*  
   Trigger: merge request events  
   Enable SSL verification  
